#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H
#include <windows.h>
#include <string>

class InputManager
{
public:
            InputManager();
    void    readInput(const float deltaTime);

    unsigned int getCursorX()  const;
    unsigned int getCursorY()  const;
    bool isPressed()  const;
    bool isReleased() const;


private:
    COORD cursor;
    bool pressed;
    bool released;
};

#endif // INPUTMANAGER_H
