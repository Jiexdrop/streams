#include "ASCIIRenderer.h"
#include "NYTimer.h"
#include "GameStateManager.h"
#include "World.h"

#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#include <chrono>

void ShowConsoleCursor( bool showFlag )
{
    HANDLE out = GetStdHandle( STD_OUTPUT_HANDLE );

    CONSOLE_CURSOR_INFO cursorInfo;
    GetConsoleCursorInfo( out, &cursorInfo );
    cursorInfo.bVisible = showFlag; // Set the cursor visibility flag

    SetConsoleCursorInfo( out, &cursorInfo );
}

void PrepareConsole()
{
    ShowConsoleCursor( false );

    // NOTE Disable quick edit otherwise the console don't receive mouse events
    SetConsoleMode( GetStdHandle( STD_INPUT_HANDLE ), ( ENABLE_PROCESSED_INPUT | ENABLE_MOUSE_INPUT | ~ENABLE_QUICK_EDIT_MODE ) );
}

int main( void )
{
    PrepareConsole();

    // Instantiate subsystems
    NYTimer appTimer;
    GameStateManager gameStateManager;
    ASCIIRenderer renderer;
    World logicWorld;
    InputManager inputManager;
    UserInterface userInterface;

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

    // Initialize stuff
    appTimer.start();
    renderer.create( 160, 40 );
    logicWorld.create( 160, 40 );
    logicWorld.generateWorld(seed);

    auto frameTime = appTimer.getElapsedSeconds();
    auto previousFrameTime = frameTime;
    while ( true ) {
        previousFrameTime = frameTime;
        frameTime = appTimer.getElapsedSeconds();
        float deltaTime = static_cast<float>( frameTime - previousFrameTime );
        
        // Logic
        logicWorld.update( frameTime );
		inputManager.readInput( deltaTime );
        userInterface.update( &logicWorld, &inputManager );

        if ( gameStateManager.shouldQuit() ) {
            break;
        }
    
        // Render
        renderer.clearBuffer( ' ', 0xe0 );

        gameStateManager.render( &renderer, &logicWorld, &userInterface, &inputManager, deltaTime );
        
        renderer.present();
        

    }

    return 0;
}
