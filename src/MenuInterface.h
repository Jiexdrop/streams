#ifndef MENUINTERFACE_H
#define MENUINTERFACE_H
#include "Button.h"
#include "Inputmanager.h"
#include "Interface.h"
#include <fstream>

#define NB_TITLE_LINES 14
#define NB_CITY_LINES 38

class GameStateManager;

struct Buttons
{
	enum Type
	{
		NONE = -1,

		PLAY,
		QUIT,

		COUNT
	};
};



class MenuInterface : Interface
{
public:
    MenuInterface();

    void render(GameStateManager *gm, ASCIIRenderer *renderer, InputManager *im);

private:
    Button buttons[Buttons::COUNT];
    std::string buttonsNames[Buttons::COUNT];
    std::string titleStrings[NB_TITLE_LINES];
    std::string titleCityStrings[NB_CITY_LINES];


    void setup();
};

#endif // MENUINTERFACE_H
