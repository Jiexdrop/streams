#include "GameStateManager.h"

GameStateManager::GameStateManager()
    : state( State::MENU )
{

}

void GameStateManager::render( ASCIIRenderer* renderer, World* logicWorld, UserInterface* userInterface, InputManager* inputManager, const float deltaTime )
{
    switch ( state ) {
    case MENU:
        menuInterface.render( this, renderer, inputManager );
        break;

    case GAME:
        // Render World
        logicWorld->renderWorld( renderer );

        // Render UI
        userInterface->render( renderer, logicWorld, inputManager, deltaTime );
        break;
    default:
        break;
    }

}

void GameStateManager::setState( State state )
{
    this->state = state;
}

State GameStateManager::getActiveState() const
{
    return state;
}

bool GameStateManager::shouldQuit() const
{
    return ( state == State::QUIT );
}
