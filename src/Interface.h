#ifndef INTERFACE_H
#define INTERFACE_H

#include "ASCIIRenderer.h"

#define INTERFACE_FILL_CHAR ':'
#define INTERFACE_FILL_COLOR 0x70
#define INTERFACE_TEXT_FILL_COLOR 0x70

class Interface
{
public:
    Interface();
    virtual ~Interface() = 0;

protected:
    bool checkCollision(int x1, int y1, int w1, int h1, int x2, int y2, int w2 , int h2);
    void drawHUDBorders( ASCIIRenderer* renderer, const int flags );

    virtual void setup() = 0;

};

#endif // INTERFACE_H
