#ifndef GAMESTATEMANAGER_H
#define GAMESTATEMANAGER_H

#include "ASCIIRenderer.h"
#include "UserInterface.h"
#include "MenuInterface.h"

enum State
{
    MENU,
    GAME,
    SETTINGS,
    QUIT
};

class GameStateManager
{
public:
    GameStateManager();
    void render(ASCIIRenderer* renderer, World* logicWorld, UserInterface* userInterface, InputManager* inputManager, const float deltaTime);
    void setState(State state);

    State getActiveState() const;
    bool shouldQuit() const;

private:
    MenuInterface menuInterface;

    State state;
};

#endif // GAMESTATEMANAGER_H
