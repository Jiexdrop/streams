#pragma once

#include <memory>
#include <string>

#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>

class ASCIIRenderer
{
public:
            ASCIIRenderer();
            ASCIIRenderer( ASCIIRenderer& ) = default;
            ASCIIRenderer& operator = ( ASCIIRenderer& ) = default;
            ~ASCIIRenderer();

    void    create(const int screenWidth, const int screenHeight);
    
    void    fillPixel( const unsigned int x, const unsigned int y, const char asciiValue, const int flags = 0x0, const bool blend = false );
    void    drawLine( const unsigned int x0, const unsigned int y0, const unsigned int x1, const unsigned int y1, const char fillValue = '%', const int flags = 0x0 );
    void    drawRectangle( const unsigned int lowX, const unsigned int lowY, const unsigned int hiX, const unsigned int hiY, const char fillValue = '%', const int flags = 0x0 );
    void    drawBlendedRectangle( const unsigned int lowX, const unsigned int lowY, const unsigned int hiX, const unsigned int hiY, const char fillValue = '%', const int flags = 0x0 );
    void    drawCenteredRectangle( const unsigned int centerX, const unsigned int centerY, const unsigned int width, const unsigned int height, const char fillValue, const int flags );
    void    drawText(const unsigned int x, const unsigned int y, const std::string& text, const int flags = 0x0 );
    void    drawUiText(const unsigned int x, const unsigned int y, const std::string& text, const unsigned int width, const int flags = 0x0 );

    void    clearBuffer();
    void    clearBuffer( const char clearValue, const int flags = 0x0 );
    void    present();

    unsigned int getBufferWidth() const;
    unsigned int getBufferHeight() const;

private:
    static constexpr int BUFFER_COUNT = 2;

private:
    std::unique_ptr<CHAR_INFO>    buffers[BUFFER_COUNT];
    int                           currentBufferIndex;
    HANDLE                        consoleHandle;
    COORD                         bufferSize;
    unsigned int                  bufferWidth;
    unsigned int                  bufferHeight;

private:
    SMALL_RECT  getWholeBufferRegion() const;
    void        clearBuffer( const int bufferIndex );
};
