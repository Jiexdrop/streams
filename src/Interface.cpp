#include "Interface.h"

Interface::Interface()
{

}

Interface::~Interface() {

}

bool Interface::checkCollision(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2){
    return x1 < x2 + w2 &&
            x1 + w1 > x2 &&
            y1 < y2 + h2 &&
            h1 + y1 > y2;
}


void Interface::drawHUDBorders( ASCIIRenderer* renderer, const int flags )
{
    const auto backbufferWidth = ( renderer->getBufferWidth() - 1 );
    const auto backbufferHeight = ( renderer->getBufferHeight() - 1 );

    renderer->drawLine( backbufferWidth, 0, backbufferWidth, backbufferHeight, INTERFACE_FILL_CHAR, flags );
    renderer->drawLine( 0, 0, 0, backbufferHeight, INTERFACE_FILL_CHAR, flags );
    renderer->drawLine( 0, 0, backbufferWidth, 0, INTERFACE_FILL_CHAR, flags );
    renderer->drawLine( 0, backbufferHeight, backbufferWidth, backbufferHeight, INTERFACE_FILL_CHAR, flags );
}
