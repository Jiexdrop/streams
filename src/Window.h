#ifndef WINDOW_H
#define WINDOW_H

#include "Button.h"
#include "Inputmanager.h"
#include "World.h"

#define MAX_CHOICES 4

#define WINDOW_HIDE -1
#define WINDOW_NEXT_LEVEL -2

class Window
{

public:
    Window();
    Window(int x, int y, int width, int height, std::string file);
    Window(int x, int y, int width, int height);

    void render(ASCIIRenderer *renderer, InputManager* im);
    void addChoice(std::string key, std::string value);
    void setScreen(const int index);
    void updateWindow();
    void clearWindow();
    void setWorld(World * logicWorld);
    bool checkCollision(int x1, int y1, int w1, int h1);

    int getActual() const;
    void setActual(int value);

private:
    int x;
    int y;
    int width;
    int height;

    int index;

    int actual;

    std::string file;

    std::string question;

    Button choices[MAX_CHOICES];
    int choicesMap[MAX_CHOICES];

    Button other;

};

#endif // WINDOW_H
