#pragma once

#include <memory>
#include <vector>

class ASCIIRenderer;

enum eAreaType : unsigned short
{
	AREA_TYPE_UNASSIGNED = 0,

	AREA_TYPE_ROAD,
	AREA_TYPE_RESIDENTIAL,
	AREA_TYPE_INDUSTRIAL,
	AREA_TYPE_COMMERCIAL,

	AREA_TYPE_WATER_STATION,
	AREA_TYPE_ELECTRIC_STATION,
	AREA_TYPE_PARC,
	AREA_TYPE_SCHOOL,

	AREA_TYPE_COUNT
};

#define RESIDENTIAL_AREA_COST 10
#define INDUSTRIAL_AREA_COST 20
#define COMMERCIAL_AREA_COST 15
#define ROAD_COST 5
#define CLEAR_COST 20

// Ratio of _ for 1 station
#define RATIO_RESIDENCES_STATION 90
#define RATIO_INDUSTRIES_STATION 120
#define RATIO_COMMERCES_STATION 105

#define X_GRID 8
#define Y_GRID 8
#define TREES_DISTANCE 4

enum Transaction
{
	DEBIT,
	CREDIT
};

enum BackgroundTile
{
	UNASSIGNED = 0,

	TREE,
	FOREST,
	LAKE,
	WASTELAND,

	BACKGROUND_TILE_COUNT
};

struct WorldEntity
{
	// Cell Type
	enum : unsigned short
	{
		EMPTY_TILE = 0,
		BACKGROUND_TILE,
		ROAD,
		BUILDING,
	} Type;

    unsigned short entityIndex;
    double level;
	unsigned int __PADDING__;

	WorldEntity()
		: Type(EMPTY_TILE)
		, entityIndex(0)
        , level(0)
	{

	}

    WorldEntity(double l, unsigned short id) : Type(EMPTY_TILE)
	{
        entityIndex = id;
		level = l;
	}


	bool operator<(const WorldEntity &we) const {
		return level > we.level;
	}

	bool operator==(const WorldEntity &we) const {
		return entityIndex == we.entityIndex && Type == we.Type;
	}

};


class World
{
public:
	World();
	World(World&) = default;
	World& operator = (World&) = default;
	~World();

	void    create(const int worldWidth = 80, const int worldHeight = 20);

	void    generateWorld(unsigned int seed = ~0);
    void    resetWorld();

	void    renderWorld(ASCIIRenderer* renderer);

	// Methods that construct the world
	void    addBackgroundTile(const int x, const int y, const BackgroundTile tile, unsigned int &seed);
    void    putBackgroundTile(const unsigned int x, const unsigned int y, const BackgroundTile tile, const char letter, unsigned int &seed);
	void	buildStation(const eAreaType areaType, const unsigned int lowX, const unsigned int lowY);
	void    addRoad(const unsigned int lowX, const unsigned lowY, const unsigned int hiX, const unsigned int hiY);
	void	deleteWorldEntity(const unsigned int x, const unsigned int y);

	void    upgradeBuilding(const int buildingInternalIndex);
	void    upgradeZone(const int zoneInternalIndex);

	int     getFunds() const;
	int     getDay() const;

	bool	adjacentToRoad(const unsigned int lowX, const unsigned lowY, const unsigned int hiX, const unsigned int hiY);
	bool	overlapsArea(const unsigned int lowX, const unsigned lowY, const unsigned int hiX, const unsigned int hiY);
	bool	isStation(const eAreaType areaType) const;

	void    buildZone(const eAreaType areaType, const unsigned int lowX, const unsigned lowY, const unsigned int hiX, const unsigned int hiY, const char initialLevel = 0);
	void    removeInZone(const unsigned int lowX, const unsigned lowY, const unsigned int hiX, const unsigned int hiY);

	int		getAreaTypeCost(const eAreaType areaType) const;
	int		getAreaTypeCount(const eAreaType areaType) const;
    float	getAreaTypeMultiplier(const eAreaType areaType) const;

    int     getLevel() const;
    void    nextLevel();

    //River generation
    bool	aStarRiver( int startX, int startY, int goalX, int goalY, unsigned int &seed);
    std::vector<WorldEntity> getNeighboors(const unsigned int x, const unsigned int y);

	// Methods that update the Funds of the game
	bool	enoughFunds(const int money);
	void	updateFunds(const Transaction transaction, const int money);
	void	computeFunds();
    int     getLevelFunds();
    void    setLevelFunds(int value);

    void	update(const float time);

    WorldEntity* getWorldTile(const unsigned int mouseX, const unsigned int mouseY) const;




private:
    int                             worldGridWidth;
    int                             worldGridHeight;
    int                             worldGridLength;
	std::unique_ptr<WorldEntity>    worldGrid;


	// Game Logic
	int                             funds;
    int                             level;
	int                             day;
    float                           nextActionTime;
    float                           period;
    int                             levelFunds;

	struct EntityBuilding {
		eAreaType		areaType;
		char            level; // NOTE Numerical value of the level (litteral value is stored in the group)
		char            groupId;
	} buildingDb[4096];
	int buildingCount;

	struct EntityRoad {
		int             flags;
		char            value;
		char            __PADDING__[3];
	} roadDb[4096];
	int roadCount;

	struct EntityBackground {
		BackgroundTile  type;
		char            value;
	} backgroundDb[1024];
	int backgroundCount;

	struct BuildingGroup
	{
		std::vector<EntityBuilding*>    buildings;
		char                            levelAverage;
	} buildingGroups[256];
	int buildingGroupCount;

private:
	int allocateBuildingGroupId();
	int allocateRoad();
	int allocateBackground();
	int allocateBuilding();

	bool isCellOccupied(const unsigned int x, const unsigned int y) const;

	WorldEntity& getTileAtCoordinates(const unsigned int x, const unsigned int y) const;
    bool inBounds(const int x, const int y) const;
	void recomputeBuildingGroupAverage(const char buildingGroupId);
};
