#include "UserInterface.h"

#undef min
#undef max
#include <algorithm>
#include <chrono>

UserInterface::UserInterface()
    : startPress({ 1,1 })
    , endPress({ 1,1 } )
    , tool(Tools::NONE)
    , selectedZoneLowX(0)
	, selectedZoneLowY(0)
	, selectedZoneHiX(0)
	, selectedZoneHiY(0)
    , win(false)
{
    setup();
}

void UserInterface::setup(){
    std::string toolsNames[Tools::COUNT];
    toolsNames[Tools::ROAD] = "Road";
    toolsNames[Tools::RESIDENTIAL] = "Resi";
    toolsNames[Tools::INDUSTRIAL] = "Indt";
    toolsNames[Tools::COMMERCIAL] = "Cmrc";

	toolsNames[Tools::WATER_STATION] = "Watr";
	toolsNames[Tools::ELECTRIC_STATION] = "Elec";
	toolsNames[Tools::PARC] = "Parc";
	toolsNames[Tools::SCHOOL] = "Schl";

	toolsNames[Tools::REMOVE] = "*Del";

    for(int i = 0; i < Tools::COUNT; i++){
        buttons[i] = Button(((i+1) * 10), 37, 8, 4, toolsNames[i]);
    }

    windows[0] = Window(80, 18, 80, 25, "assets/story.txt");
    windows[1] = Window(80, 18, 80, 15);
}

void UserInterface::drawInterfaceLayout(ASCIIRenderer* renderer) {
    const auto backbufferWidth = ( renderer->getBufferWidth() );
    const auto backbufferHeight = ( renderer->getBufferHeight() );

    // ToolBar
    renderer->drawRectangle(1, 34, backbufferWidth - 1, 37 + 4, INTERFACE_FILL_CHAR, INTERFACE_FILL_COLOR);

    // MoneyBar
    renderer->drawRectangle(120, 1, backbufferWidth - 1, 3, INTERFACE_FILL_CHAR, INTERFACE_FILL_COLOR);
}

void UserInterface::winInterfaceUpdate(World * logicWorld){
    if(logicWorld->getFunds()>=logicWorld->getLevelFunds() && win == false){
        win = true;
        windows[Windows::WIN].setScreen(0);
        windows[Windows::WIN].setWorld(logicWorld);
    }

    if(windows[Windows::WIN].getActual() == WINDOW_NEXT_LEVEL) {
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

        logicWorld->setLevelFunds(logicWorld->getLevelFunds() + logicWorld->getFunds());
        logicWorld->resetWorld();
        logicWorld->generateWorld(seed);
        windows[Windows::WIN].setScreen(WINDOW_HIDE);
        win = false;
    }
}

void UserInterface::render(ASCIIRenderer* renderer, World * logicWorld, InputManager* im, const float deltaTime) {

    // Active Selection Rectangle
    renderer->drawBlendedRectangle( selectedZoneLowX, selectedZoneLowY, selectedZoneHiX, selectedZoneHiY, '#', FOREGROUND_RED );


    drawInterfaceLayout( renderer );

    for(int i = 0; i < Tools::COUNT; i++){
        buttons[i].render(renderer);
    }

    drawHUDBorders( renderer, INTERFACE_FILL_COLOR );


    // User Interface
    renderer->drawText( 116, 35, "ROADS: " + std::to_string(logicWorld->getAreaTypeCount(eAreaType::AREA_TYPE_ROAD)), INTERFACE_TEXT_FILL_COLOR );
    renderer->drawText( 116, 36, "RESIDENCES: " + std::to_string(logicWorld->getAreaTypeCount(eAreaType::AREA_TYPE_RESIDENTIAL)), INTERFACE_TEXT_FILL_COLOR );
    renderer->drawText(	116, 37, "COMMERCES: " + std::to_string(logicWorld->getAreaTypeCount(eAreaType::AREA_TYPE_COMMERCIAL)), INTERFACE_TEXT_FILL_COLOR );
    renderer->drawText( 116, 38, "INDUSTRIES: " + std::to_string(logicWorld->getAreaTypeCount(eAreaType::AREA_TYPE_INDUSTRIAL)), INTERFACE_TEXT_FILL_COLOR );
    renderer->drawText( 136, 38, "SCHOOLS: " + std::to_string(logicWorld->getAreaTypeCount(eAreaType::AREA_TYPE_SCHOOL)), INTERFACE_TEXT_FILL_COLOR );
    renderer->drawText( 136, 37, "PARCS: " + std::to_string(logicWorld->getAreaTypeCount(eAreaType::AREA_TYPE_PARC)), INTERFACE_TEXT_FILL_COLOR );
    renderer->drawText( 136, 36, "ELECTRIC STATIONS: " + std::to_string(logicWorld->getAreaTypeCount(eAreaType::AREA_TYPE_ELECTRIC_STATION)), INTERFACE_TEXT_FILL_COLOR );
    renderer->drawText( 136, 35, "WATER STATIONS: " + std::to_string(logicWorld->getAreaTypeCount(eAreaType::AREA_TYPE_WATER_STATION)), INTERFACE_TEXT_FILL_COLOR );
    renderer->drawText( 122, 1, "MONEY: " + std::to_string(logicWorld->getFunds()), INTERFACE_TEXT_FILL_COLOR );
    renderer->drawText( 142, 1, "DAY: " + std::to_string(logicWorld->getDay()), INTERFACE_TEXT_FILL_COLOR );

    // Game logic updates interface
    winInterfaceUpdate(logicWorld);

    //DEBUG
    /*
    renderer->drawText( 0, 0, "FPS: " + std::to_string( 1.0f / deltaTime ) + " (" + std::to_string( deltaTime ) + "ms)", INTERFACE_TEXT_FILL_COLOR );
    renderer->drawText( 0, 1, "CURSOR: " + cursorPos, INTERFACE_TEXT_FILL_COLOR );
    std::string hoveredTileInfos = "No Tile Hovered...";
    auto* hoveredTile = logicWorld->getWorldTile( im->getCursorX(), im->getCursorY() );
    if ( hoveredTile != nullptr ) {
        switch ( hoveredTile->Type ) {
        case WorldEntity::EMPTY_TILE:
            hoveredTileInfos = "Empty Tile - L: " + std::to_string(hoveredTile->level) + " ID: " + std::to_string(hoveredTile->entityIndex);
            break;
        case WorldEntity::BACKGROUND_TILE:
            hoveredTileInfos = "Background Tile - L: " + std::to_string(hoveredTile->level) + " ID: " + std::to_string(hoveredTile->entityIndex);
            break;
        case WorldEntity::ROAD:
            hoveredTileInfos = "Road - L: " + std::to_string(hoveredTile->level) + " ID: " + std::to_string(hoveredTile->entityIndex);
            break;
        case WorldEntity::BUILDING:
            hoveredTileInfos = "Building - L: " + std::to_string(hoveredTile->level) + " ID: " + std::to_string(hoveredTile->entityIndex);
            logicWorld->upgradeBuilding( hoveredTile->entityIndex );
            break;
        }
    }

    renderer->drawText( 48, 0, "HOVERED: " + hoveredTileInfos, INTERFACE_TEXT_FILL_COLOR );*/

    for(int i = 0; i<WINDOW_COUNT; i++){
        windows[i].render(renderer, im);
    }

}

void UserInterface::update(World *world, InputManager* im){
    cursorPos = std::to_string( im->getCursorX() ) + "-" + std::to_string( im->getCursorY() ) + " | LEFT BUTTON: " + ( ( im->isPressed() ) ? "PRESSED " : "RELEASED" );

    unsigned int minX = std::min(im->getCursorX(), static_cast<unsigned int>(startPress.X));
    unsigned int minY = std::min(im->getCursorY(), static_cast<unsigned int>(startPress.Y));
    unsigned int maxX = std::max(im->getCursorX(), static_cast<unsigned int>(startPress.X));
    unsigned int maxY = std::max(im->getCursorY(), static_cast<unsigned int>(startPress.Y));

    if(im->isPressed()){
        for(int i = 0; i < Tools::COUNT; i++){
			buttons[i].updateCost(world->getAreaTypeCost((eAreaType)(i)));
			if (buttons[i].checkCollision(im->getCursorX(), im->getCursorY(), 1, 1)) {
				tool = static_cast<Tools>(i);
                for (int j = 0; j < Tools::COUNT; j++) // reset
				{
					buttons[j].checked = false;
				}
				buttons[i].checked = true;
			}
        }

        if(startPress.X == -1 && startPress.Y == -1){
            startPress.Y = im->getCursorY();
            startPress.X = im->getCursorX();

        } else {
            cursorPos.append( std::to_string(minX) + "-"  + std::to_string(minY) + "/" + std::to_string(maxX) + "-" + std::to_string(maxY));

            switch (tool) {
            case ROAD:
                if(maxX - minX >= maxY - minY ){ // If width > height
                    selectedZoneLowX = minX;
                    selectedZoneLowY = startPress.Y;
                    selectedZoneHiX = maxX;
                    selectedZoneHiY = startPress.Y + 1;
                } else { // if height > width
                    selectedZoneLowX = startPress.X;
                    selectedZoneLowY = minY;
                    selectedZoneHiX = startPress.X + 1;
                    selectedZoneHiY = maxY;
                }
                break;
            default:
                selectedZoneLowX = minX;
                selectedZoneLowY = minY;
                selectedZoneHiX = maxX;
                selectedZoneHiY = maxY;
                break;
            }
        }
    } else {

        if ( startPress.X != -1 && startPress.Y != -1 ) {
            if ( im->isReleased() ) {
                int bX = minX;
                int bY = minY;
                int bW = maxX; // -minX;
                int bH = maxY; // -minY;

                if ( im->getCursorX() != startPress.X && im->getCursorY() != startPress.Y ) {
					switch (tool) {
					case NONE:
						break;
					case RESIDENTIAL:
						world->buildZone(static_cast<eAreaType>(tool), bX, bY, bW, bH);
						break;
					case INDUSTRIAL:
						world->buildZone(static_cast<eAreaType>(tool), bX, bY, bW, bH);
						break;
					case COMMERCIAL:
						world->buildZone(static_cast<eAreaType>(tool), bX, bY, bW, bH);
						break;
					case REMOVE:
						world->removeInZone(bX, bY, bW, bH);
						break;
					default:	
						break;
					}
                } else if ( tool == ROAD ) {
                    world->addRoad( selectedZoneLowX, selectedZoneLowY, selectedZoneHiX, selectedZoneHiY );
				} else if ( tool == WATER_STATION || tool == ELECTRIC_STATION || tool == SCHOOL || tool == PARC) { 
					world->buildStation(static_cast<eAreaType>(tool), startPress.X, startPress.Y);
				} 
                startPress.X = -1;
                startPress.Y = -1;
                selectedZoneLowX = 0;
                selectedZoneLowY = 0;
                selectedZoneHiX = 0;
                selectedZoneHiY = 0;
            }
        }
		if (tool == WATER_STATION
			|| tool == ELECTRIC_STATION
			|| tool == SCHOOL
			|| tool == PARC) {
			selectedZoneLowX = im->getCursorX() -1;
			selectedZoneLowY = im->getCursorY() - 1;
			selectedZoneHiX = im->getCursorX() + 2;
			selectedZoneHiY = im->getCursorY() + 1;
		}

    }
}

