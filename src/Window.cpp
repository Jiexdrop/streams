#include "Window.h"
#include "Interface.h"
#include <fstream>
#include <sstream>

Window::Window(int x, int y, int width, int height, std::string file)
    : x(x)
    , y(y)
    , width(width)
    , height(height)
    , index(0)
    , actual(0)
    , file(file)
    , question()
{
    updateWindow();
}

Window::Window(int x, int y, int width, int height)
    :x(x)
    , y(y)
    , width(width)
    , height(height)
    , index(0)
    , actual(0)
    , file("NONE")
    , question()
{
    setScreen(WINDOW_HIDE);
    addChoice("NEXT LEVEL", std::to_string(WINDOW_NEXT_LEVEL));
    addChoice("HIDE", std::to_string(WINDOW_HIDE));
}

Window::Window(){
    x = 0;
    y = 0;
    width = 0;
    height = 0;
    question = "";
    file = "";
    index = 0;
    actual = 0;

    addChoice("","");
    addChoice("","");
    addChoice("","");
    addChoice("","");
}

void Window::addChoice(std::string key, std::string value) {
    if(index < MAX_CHOICES){
        choices[index] = Button(x, (y + (index * 5)) - height/4 , 50, 4, key);
        choicesMap[index] = atoi( value.c_str() );
        index++;
    }
}

void Window::setScreen(const int index) {
    actual = index;
    updateWindow();
}

void Window::setWorld(World * logicWorld) {

    question = "Level " + std::to_string(logicWorld->getLevel()) + ". ";
    question += "You won at day " + std::to_string(logicWorld->getDay());
    question += " with " + std::to_string(logicWorld->getFunds()) + " funds.";

    logicWorld->nextLevel();
}

int Window::getActual() const
{
    return actual;
}

void Window::setActual(int value)
{
    actual = value;
}

void Window::updateWindow() {
    if(file != "NONE"){
        clearWindow();
        std::ifstream titleText(file);
        bool fill = false;
        for(std::string line; getline( titleText, line ); )
        {
            std::istringstream is_line(line);
            std::string key;
            if( std::getline(is_line, key, '=') )
            {

                std::string value;
                if( std::getline(is_line, value) ){
                    if (key == "WINDOW"){
                        int n = atoi( value.c_str() );
                        n == actual ? fill = true : fill = false;

                    } else if (key == "TITLE"){
                        if(fill) question = value;
                    } else {
                        if(fill) addChoice(key,value);
                    }
                }

            }
        }
    }
}

void Window::clearWindow() {
    for(int i = 0; i < MAX_CHOICES; i++){
        choices[i] = Button();
        choicesMap[i] = 0;
        index = 0;
    }
}

void Window::render(ASCIIRenderer *renderer, InputManager* im)
{
    if(actual >= 0){ // RENDER
        renderer->drawCenteredRectangle( x, y, width, height, ' ', INTERFACE_FILL_COLOR);
        renderer->drawUiText(x - width/2 + 1, y - height/2, question, width * 2, INTERFACE_FILL_COLOR);

        if(im->isPressed()){
            for(int i = 0; i < MAX_CHOICES; i++){
                if (choices[i].checkCollision(im->getCursorX(), im->getCursorY(), 1, 1)) {
                    for (int j = 0; j < MAX_CHOICES; j++) // reset
                    {
                        choices[j].checked = false;
                    }
                    choices[i].checked = true;
                    actual = choicesMap[i];
                }
            }
        }

        if(im->isReleased()){
            updateWindow();
        }

        for(int i = 0; i < MAX_CHOICES; i++){
            choices[i].render(renderer);
        }

        renderer->drawLine( x - (width/2), y - (height/2) - 1, width + (width/2) - 1,  y - (height/2) - 1, INTERFACE_FILL_CHAR,  INTERFACE_FILL_COLOR);
    }
}
