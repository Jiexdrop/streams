#ifndef BUTTON_H
#define BUTTON_H
#include "ASCIIRenderer.h"

#define BUTTON_CHECKED_FILL_COLOR 0xf0
#define BUTTON_FILL_COLOR 0x8f

class Button
{
public:
    Button();
    Button(int x, int y, int width, int height, std::string name);

    void render(ASCIIRenderer *renderer);
    bool checkCollision(int x1, int y1, int w1, int h1);
	void updateCost(const int cost);

    bool checked;

private:
    int x;
    int y;
    int width;
    int height;

    void (*callback)();

    std::string name;
    std::string cost;
};

#endif // BUTTON_H
