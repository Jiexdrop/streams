#include "World.h"

#include "ASCIIRenderer.h"

#undef min
#undef max
#include <algorithm>
#include <random>
#include <queue>
#include <limits>
#include <cmath>

#define clamp( x, low, high ) std::max( std::min( x, high ), low )

// L_\inf norm (diagonal distance)
float linf_norm(int i0, int j0, int i1, int j1) {
    return std::max(std::abs(i0 - i1), std::abs(j0 - j1));
}


World::World()
    : worldGridWidth(0)
    , worldGridHeight(0)
    , worldGridLength(0)
    , worldGrid(nullptr)
    , funds(10000)
    , level(1)
    , day(0)
    , nextActionTime(0)
    , period(1.2f)
    , levelFunds(20000)
    , buildingCount(0)
    , roadCount(0)
    , backgroundCount(0)
    , buildingGroupCount(0)
{

}

World::~World()
{
    worldGridWidth = 0;
    worldGridHeight = 0;
    worldGridLength = 0;
    buildingCount = 0;
    roadCount = 0;
    buildingGroupCount = 0;
    backgroundCount = 0;
    funds = 0;
    day = 0;
    period = 0;
    level = 1;
}

void World::create(const int worldWidth, const int worldHeight)
{
    worldGrid.reset(new WorldEntity[worldWidth * worldHeight]);

    worldGridWidth = worldWidth;
    worldGridHeight = worldHeight;

    worldGridLength = (worldGridWidth * worldGridHeight);
}

#include "PerlinNoise.hpp"

void World::generateWorld(unsigned int seed)
{
    double frequency = 32.0;
    int octaves = 4;

    const siv::PerlinNoise perlin(seed);
    const double fx = worldGridWidth / frequency;
    const double fy = worldGridHeight / frequency;

    const int columns = worldGridWidth / X_GRID;
    const int rows = worldGridHeight / Y_GRID;

    std::mt19937					gen(seed);
    std::uniform_int_distribution<> dis(-TREES_DISTANCE, TREES_DISTANCE); // Distance between trees


    constexpr double TREE_THRESHOLD = 0.5;
    constexpr double FOREST_THRESHOLD = 0.3;
    constexpr double WASTELAND_THRESHOLD = 0.2;


    for (int x = 0; x < worldGridWidth; ++x) {
        for (int y = 0; y < worldGridHeight; ++y) {
            double noise = perlin.octaveNoise0_1(x / fx, y / fy, octaves);
            auto& cellInstance = getTileAtCoordinates(x, y);
            cellInstance.level = noise;
            cellInstance.entityIndex = ((x * worldGridHeight) + y);
        }
    }

    for (int ix = 0; ix < columns; ix++) {
        for (int iy = 0; iy < rows; iy++) {
            double noise = perlin.octaveNoise0_1(ix / fx, iy / fy, octaves);

            int x = clamp(ix * X_GRID + dis(gen), 0, worldGridWidth);
            int y = clamp(iy * Y_GRID + dis(gen), 0, worldGridHeight);

            if (noise < WASTELAND_THRESHOLD) {
                addBackgroundTile(x, y, BackgroundTile::WASTELAND, seed);
            }
            if (noise < FOREST_THRESHOLD) {
                addBackgroundTile(x, y, BackgroundTile::FOREST, seed);
            }
            if (noise < TREE_THRESHOLD) {
                addBackgroundTile(x, y, BackgroundTile::TREE, seed);
            }
        }
    }


    std::uniform_int_distribution<> riverStart(1, 33);
    std::uniform_int_distribution<> riverEnd(1, 33);
    aStarRiver(1 + riverEnd(gen), riverStart(gen), 158 - riverStart(gen), riverEnd(gen), seed);

}

void World::renderWorld(ASCIIRenderer* renderer)
{
    static constexpr int BACKGROUND_YELLOW = 0x0060;
    static constexpr int BACKGROUND_GRAY = 0x0060;
    static constexpr int BACKGROUND_AQUA = 0x0030;
    static constexpr int BACKGROUND_PURPLE = 0x050;
    static constexpr int BACKGROUND_LIME = 0x0a0;
    static constexpr int BACKGROUND_BLACK = 0x0000;

    static constexpr int AREA_FLAGSET[AREA_TYPE_COUNT] = {
        0xFFFF,

        BACKGROUND_GRAY,	// ROAD

        BACKGROUND_GREEN,	// RESIDENTIAL
        BACKGROUND_YELLOW,	// INDUSTRIAL
        BACKGROUND_BLUE,	// COMMERCIAL

        BACKGROUND_AQUA,	// WATER
        BACKGROUND_RED,		// ELECTRICITY

        BACKGROUND_LIME,	// PARC
        BACKGROUND_PURPLE,	// SCHOOL
    };

    static constexpr int BACKGROUND_FLAGSET[BACKGROUND_TILE_COUNT] = {
        0xFFFF,
        BACKGROUND_LIME,	// TREE
        BACKGROUND_GREEN,	// FOREST
        BACKGROUND_BLUE,	// LAKE
        BACKGROUND_GRAY,	// WASTELAND
    };

    for (int cellIdx = 0; cellIdx < worldGridLength; cellIdx++) {
        const auto cell = worldGrid.get()[cellIdx];

        if (cell.Type == WorldEntity::EMPTY_TILE) {
            continue;
        }

        const int colIdx = (cellIdx % worldGridHeight);
        const int rowIdx = (cellIdx / worldGridHeight);

        char cellValue = 0;
        int cellFlagset = 0;

        switch (cell.Type) {
        case WorldEntity::BACKGROUND_TILE: {
            const auto& background = backgroundDb[cell.entityIndex];

            cellFlagset = BACKGROUND_FLAGSET[background.type];
            cellValue = background.value;
            break;
        }
        case WorldEntity::ROAD: {
            const auto& road = roadDb[cell.entityIndex];

            cellFlagset = road.flags;
            cellValue = road.value;
            break;
        }
        case WorldEntity::BUILDING: {
            const auto& building = buildingDb[cell.entityIndex];

            cellFlagset = AREA_FLAGSET[building.areaType];
            cellValue = buildingGroups[building.groupId].levelAverage;
            break;
        }
        case WorldEntity::EMPTY_TILE: {
            break;
        }
        }

        renderer->fillPixel(rowIdx, colIdx, cellValue, cellFlagset);
    }
}

WorldEntity& World::getTileAtCoordinates(const unsigned int x, const unsigned int y) const
{
    return worldGrid.get()[((x * worldGridHeight) + y)];
}

void World::addBackgroundTile(const int x, const int y, const BackgroundTile tile, unsigned int &seed)
{
    auto& iCell = getTileAtCoordinates(x, y);

    std::mt19937 gen(seed);
    std::uniform_int_distribution<> forestDis(3, 9); // Radius of forest
    std::uniform_int_distribution<> wasteDis(0, 3); // Size of waste
    std::uniform_int_distribution<> lakeDis(0, 2); // Radius of lake
    std::uniform_int_distribution<> treesDis(0, 3); // Number of trees

    if (iCell.Type == WorldEntity::EMPTY_TILE) {

        iCell.entityIndex = allocateBackground();

        iCell.Type = WorldEntity::BACKGROUND_TILE;

        auto& background = backgroundDb[iCell.entityIndex];
        background.type = tile;

        const siv::PerlinNoise perlin(seed);

        switch (tile)
        {
        case LAKE: {
            background.value = ' ';
            int lakeRadius = lakeDis(gen);

            for (int j = -lakeRadius; j <= lakeRadius; j++) {
                for (int i = -lakeRadius; i <= lakeRadius; i++) {
                    if (i*i + j * j <= lakeRadius * lakeRadius) {
                        putBackgroundTile(x + i, y + j, tile, background.value, seed);
                        seed++;
                    }
                }
            }
            break;
        }
        case FOREST: {
            background.value = ' ';
            const siv::PerlinNoise perlin(seed);
            double frequency = 32.0;
            int octaves = 4;
            const double fx = worldGridWidth / frequency;
            const double fy = worldGridHeight / frequency;
            int forestRadius = forestDis(gen);

            for (int j = -forestRadius; j <= forestRadius; j++) {
                for (int i = -forestRadius; i <= forestRadius; i++) {
                    if (i*i + j * j <= forestRadius * forestRadius) {
                        double noise = perlin.octaveNoise0_1(i / fx, j / fy, octaves);
                        if (noise < 0.65f) {
                            putBackgroundTile(x + i, y + j, tile, background.value, seed);
                            seed++;
                        }
                    }
                }
            }
            break;
        }
        case WASTELAND: {
            background.value = ' ';
            int wasteland = wasteDis(gen);

            for (int j = 0; j < wasteland; j++) {
                for (int i = 0; i < wasteland; i++) {
                    if( i % 2 == j % 2 ){
                        putBackgroundTile(x + i, y + j, tile, background.value, seed);
                        seed++;
                    }
                }
            }
            break;
        }
        case TREE:{
            background.value = ' ';
            int trees = treesDis(gen);

            for (int j = 0; j <= trees; j++) {
                for (int i = 0; i <= trees; i++) {
                    if( i % 2 == j % 2 ){
                        putBackgroundTile(x + i, y + j, tile, background.value, seed);
                        seed++;
                    }
                }
            }
            break;
        }
        }
    }
}

bool World::aStarRiver( int startX, int startY, int goalX, int goalY, unsigned int &seed ) {
    const float INF = std::numeric_limits<float>::infinity();

    WorldEntity start = getTileAtCoordinates( startX, startY );
    WorldEntity goal = getTileAtCoordinates( goalX, goalY );
    start.level = 0.;
    goal.level = 0.;

    int h = worldGridHeight;
    int x = start.entityIndex / h;
    int y = start.entityIndex % h;

    float* costs = new float[worldGridLength] { INF };
    for (int i = 0; i < worldGridLength; ++i)
        costs[i] = INF;
    costs[start.entityIndex] = 0.;

    std::priority_queue<WorldEntity> nodes_to_visit;
    nodes_to_visit.push(start);

    std::vector<WorldEntity> neighboors;


    bool* visited = new bool[worldGridLength] { false };
    for ( int i = 0; i < worldGridLength; ++i )
        visited[i] = false;

    bool solution_found = false;
    while (!nodes_to_visit.empty()) {
        WorldEntity cur = nodes_to_visit.top();

        if (cur == goal) {
            solution_found = true;
            break;
        }

        nodes_to_visit.pop();
        x = cur.entityIndex / h;
        y = cur.entityIndex % h;

        float heuristic_cost;

        neighboors = getNeighboors(x, y);
        for (auto ne : neighboors) {
            // the sum of the cost so far and the cost of this move

            if(visited[ne.entityIndex]) continue; //If neighboor not visited

            float new_cost = costs[cur.entityIndex] + getTileAtCoordinates((unsigned int)( ne.entityIndex / h ), (unsigned int)( ne.entityIndex % h ) ).level; // weight of nbrs[i]

            if (new_cost < costs[ne.entityIndex]) {
                // estimate the cost to the goal based on legal moves
                heuristic_cost = linf_norm((int)ne.entityIndex / h, (int)ne.entityIndex % h, goalX, goalY);

                // paths with lower expected cost are explored first
                float priority = new_cost + heuristic_cost;
                nodes_to_visit.push(WorldEntity(priority, ne.entityIndex));

                costs[ne.entityIndex] = new_cost;
                visited[ne.entityIndex] = true;

                putBackgroundTile(x, y, BackgroundTile::LAKE, ' ', seed);
            }
        }
    }

    delete [] costs;
    delete [] visited;

    return solution_found;
}

bool World::inBounds(const int x, const int y) const
{
    return x < worldGridWidth && y < worldGridHeight && x > 0 && y > 0;
}

std::vector<WorldEntity> World::getNeighboors(const unsigned int x, const unsigned int y) {
    std::vector<WorldEntity> neighboors;

    if(inBounds(x+1, y+1)){
        neighboors.push_back(getTileAtCoordinates(x + 1, y + 1));
    }
    if(inBounds(x, y+1)){
        neighboors.push_back(getTileAtCoordinates(x, y + 1));
    }
    if(inBounds(x+1, y)){
        neighboors.push_back(getTileAtCoordinates(x + 1, y));
    }
    if(inBounds(x, y - 1)){
        neighboors.push_back(getTileAtCoordinates(x, y - 1));
    }
    if(inBounds(x - 1, y - 1)){
        neighboors.push_back(getTileAtCoordinates(x - 1, y - 1));
    }
    if(inBounds(x - 1 , y)){
        neighboors.push_back(getTileAtCoordinates(x - 1, y));
    }
    if(inBounds(x+1, y-1)){
        neighboors.push_back(getTileAtCoordinates(x + 1, y - 1));
    }
    if(inBounds(x-1, y+1)){
        neighboors.push_back(getTileAtCoordinates(x - 1, y + 1));
    }
    return neighboors;
}

void World::putBackgroundTile(const unsigned int x, const unsigned int y, const BackgroundTile tile, const char letter, unsigned int &seed)
{
    if (y > worldGridHeight || x > worldGridWidth ) return;

    auto& iCell = getTileAtCoordinates(x, y);

    if (iCell.Type == WorldEntity::EMPTY_TILE) {
        iCell.Type = WorldEntity::BACKGROUND_TILE;
        iCell.entityIndex = allocateBackground();

        auto& iBackground = backgroundDb[iCell.entityIndex];
        iBackground.type = tile;
        iBackground.value = letter;
    }
}

void World::buildZone(const eAreaType areaType, const unsigned int lowX, const unsigned lowY, const unsigned int hiX, const unsigned int hiY, const char initialLevel)
{
    const auto buildingGroup = allocateBuildingGroupId();

    auto clampedHighX = std::min(hiX, static_cast<unsigned int>(worldGridWidth));
    auto clampedHighY = std::min(hiY, static_cast<unsigned int>(worldGridHeight));

    for (unsigned int x = lowX; x < clampedHighX; x++) {
        for (unsigned int y = lowY; y < clampedHighY; y++) {
            int cost = getAreaTypeCost(areaType);

            if (enoughFunds(cost) && adjacentToRoad(x, y, x + 1, y + 1)) {
                updateFunds(Transaction::DEBIT, cost);

                auto& cellInstance = getTileAtCoordinates(x, y);
                if (cellInstance.Type != WorldEntity::EMPTY_TILE) {
                    continue;
                }

                cellInstance.Type = WorldEntity::BUILDING;
                cellInstance.entityIndex = allocateBuilding();

                auto& building = buildingDb[cellInstance.entityIndex];
                building.areaType = areaType;
                building.level = initialLevel;
                building.groupId = buildingGroup;

                buildingGroups[buildingGroup].buildings.push_back(&building);
            }
        }
    }

    recomputeBuildingGroupAverage(buildingGroup);
}

void World::removeInZone(const unsigned int lowX, const unsigned lowY, const unsigned int hiX, const unsigned int hiY)
{
    for (unsigned int x = lowX; x < hiX; x++) {
        for (unsigned int y = lowY; y < hiY; y++) {
            deleteWorldEntity(x, y);
        }
    }
}

void World::update(const float time)
{
    if (time > nextActionTime) { // We update the world each day
        day++;

        for (int i = 0; i < buildingGroupCount; i++) {
            upgradeZone(i);
        }

        computeFunds();

        nextActionTime += period;
    }
}

void World::computeFunds() {
    double maintenanceMultiplier = 1.2;
    double maintenanceMultiplierStations = 0.1;

    // MAINTENANCE
    updateFunds(Transaction::DEBIT, getAreaTypeCount(AREA_TYPE_ROAD) * (getAreaTypeCost(AREA_TYPE_ROAD) * maintenanceMultiplier));
    updateFunds(Transaction::DEBIT, getAreaTypeCount(AREA_TYPE_SCHOOL) * (getAreaTypeCost(AREA_TYPE_SCHOOL) * maintenanceMultiplier));
    updateFunds(Transaction::DEBIT, getAreaTypeCount(AREA_TYPE_PARC) * (getAreaTypeCost(AREA_TYPE_PARC) * maintenanceMultiplier));
    updateFunds(Transaction::DEBIT, getAreaTypeCount(AREA_TYPE_ELECTRIC_STATION) * (getAreaTypeCost(AREA_TYPE_ELECTRIC_STATION) * maintenanceMultiplierStations));
    updateFunds(Transaction::DEBIT, getAreaTypeCount(AREA_TYPE_WATER_STATION) * (getAreaTypeCost(AREA_TYPE_WATER_STATION) * maintenanceMultiplierStations));

    // TAXES
    updateFunds(Transaction::CREDIT, getAreaTypeCount(AREA_TYPE_RESIDENTIAL) * (getAreaTypeCost(AREA_TYPE_RESIDENTIAL) * getAreaTypeMultiplier(AREA_TYPE_RESIDENTIAL)));
    updateFunds(Transaction::CREDIT, getAreaTypeCount(AREA_TYPE_COMMERCIAL) * (getAreaTypeCost(AREA_TYPE_COMMERCIAL) * getAreaTypeMultiplier(AREA_TYPE_COMMERCIAL)));
    updateFunds(Transaction::CREDIT, getAreaTypeCount(AREA_TYPE_INDUSTRIAL) * (getAreaTypeCost(AREA_TYPE_INDUSTRIAL) * getAreaTypeMultiplier(AREA_TYPE_INDUSTRIAL)));
}

int World::getLevelFunds() {
    return levelFunds;
}

void World::buildStation(const eAreaType areaType, const unsigned int lowX, const unsigned int lowY)
{
    int cost = getAreaTypeCost(areaType);

    if (enoughFunds(cost)
            && adjacentToRoad(lowX - 1, lowY - 1, lowX + 2, lowY + 1)
            && !overlapsArea(lowX - 1, lowY - 1, lowX + 2, lowY + 1))
    {
        const auto buildingGroup = allocateBuildingGroupId();

        updateFunds(Transaction::DEBIT, cost);

        for (unsigned int x = lowX - 1; x <= lowX + 1; x++) {
            for (unsigned int y = lowY - 1; y < lowY + 1; y++) {

                auto& cellInstance = getTileAtCoordinates(x, y);
                if (cellInstance.Type != WorldEntity::EMPTY_TILE) {
                    continue;
                }

                cellInstance.Type = WorldEntity::BUILDING;
                cellInstance.entityIndex = allocateBuilding();

                auto& building = buildingDb[cellInstance.entityIndex];
                building.areaType = areaType;
                building.level = 0;
                building.groupId = buildingGroup;

                buildingGroups[buildingGroup].buildings.push_back(&building);
            }
        }

        recomputeBuildingGroupAverage(buildingGroup);
    }
}

void World::addRoad(const unsigned int lowX, const unsigned lowY, const unsigned int hiX, const unsigned int hiY)
{
    const int width = hiX - lowX;
    const int height = hiY - lowY;

    int cost = (width * height) * ROAD_COST;
    if (enoughFunds(cost)) {
        updateFunds(Transaction::DEBIT, cost);
        if (height >= width) {
            for (unsigned int y = lowY; y < hiY; y++) {
                if (!isCellOccupied(lowX, y)) {
                    auto& cellInstance = getTileAtCoordinates(lowX, y);
                    cellInstance.Type = WorldEntity::ROAD;
                    cellInstance.entityIndex = allocateRoad();

                    auto& road = roadDb[cellInstance.entityIndex];
                    road.flags = 0x80;
                    road.value = ' ';
                }
            }
        }
        else {
            for (unsigned int x = lowX; x < hiX; x++) {
                if (!isCellOccupied(x, lowY)) {
                    auto& cellInstance = getTileAtCoordinates(x, lowY);
                    cellInstance.Type = WorldEntity::ROAD;
                    cellInstance.entityIndex = allocateRoad();

                    auto& road = roadDb[cellInstance.entityIndex];
                    road.flags = 0x80;
                    road.value = ' ';
                }
            }
        }
    }
}

void World::deleteWorldEntity(const unsigned int x, const unsigned int y)
{
    auto& hoveredTile = getTileAtCoordinates(x, y);

    if (hoveredTile.Type == WorldEntity::ROAD) {

        auto& road = roadDb[roadCount];
        roadDb[hoveredTile.entityIndex] = road;
        roadCount--;
        hoveredTile.Type = WorldEntity::EMPTY_TILE;
        hoveredTile.entityIndex = roadCount;

        if (getTileAtCoordinates(x, y - 1).Type == WorldEntity::BUILDING) {
            deleteWorldEntity(x, y - 1);
        }

        if (getTileAtCoordinates(x, y + 1).Type == WorldEntity::BUILDING) {
            deleteWorldEntity(x, y + 1);
        }

        if (getTileAtCoordinates(x + 1, y).Type == WorldEntity::BUILDING) {
            deleteWorldEntity(x + 1, y);
        }

        if (getTileAtCoordinates(x - 1, y).Type == WorldEntity::BUILDING) {
            deleteWorldEntity(x - 1, y);
        }

    }
    else if (hoveredTile.Type == WorldEntity::BUILDING) {
        auto& building = buildingDb[buildingCount];
        buildingGroups[hoveredTile.entityIndex].buildings.clear();
        buildingGroups[hoveredTile.entityIndex] = buildingGroups[buildingGroupCount];
        buildingDb[hoveredTile.entityIndex] = building;

        hoveredTile.Type = WorldEntity::EMPTY_TILE;
        hoveredTile.entityIndex = buildingCount;

        buildingGroupCount--;
        buildingCount--;
    }
    else if (hoveredTile.Type == WorldEntity::BACKGROUND_TILE) {
        auto& background = backgroundDb[backgroundCount];
        backgroundDb[hoveredTile.entityIndex] = background;

        hoveredTile.Type = WorldEntity::EMPTY_TILE;
        hoveredTile.entityIndex = backgroundCount;

        backgroundCount--;

        updateFunds(Transaction::DEBIT, CLEAR_COST);
    }
}

static constexpr char MAX_UPGRADE_LEVEL = 9;

void World::upgradeBuilding(const int buildingInternalIndex)
{
    if (buildingInternalIndex < 0
            || buildingInternalIndex >= 1024) {
        return;
    }

    auto& building = buildingDb[buildingInternalIndex];
    if (building.level == MAX_UPGRADE_LEVEL) {
        return;
    }

    if (isStation(building.areaType)) return;

    building.level++;

    recomputeBuildingGroupAverage(building.groupId);
}

void World::resetWorld(){
    for (int i = 0; i < buildingGroupCount; ++i) {
        buildingGroups[i].buildings.clear();
    }
    buildingGroupCount = 0;
    buildingCount = 0;
    roadCount = 0;

    backgroundCount = 0;

    for (int x = 0; x < worldGridWidth; ++x) {
        for (int y = 0; y < worldGridHeight; ++y) {
            auto& cellInstance = getTileAtCoordinates(x, y);
            cellInstance.entityIndex = ((x * worldGridHeight) + y);
            cellInstance.Type = WorldEntity::EMPTY_TILE;
        }
    }

}

void World::upgradeZone(const int zoneInternalIndex)
{
    if (zoneInternalIndex < 0
            || zoneInternalIndex >= 256) {
        return;
    }

    auto& zone = buildingGroups[zoneInternalIndex];

    if (zone.levelAverage == MAX_UPGRADE_LEVEL) {
        return;
    }

    if (zone.buildings.empty()) {
        return;
    }

    auto& areaType = zone.buildings.front()->areaType;

    if (isStation(areaType)) return;

    switch (areaType)
    {
    case AREA_TYPE_RESIDENTIAL: {
        int ratio = RATIO_RESIDENCES_STATION;
        int totalStations = getAreaTypeCount(AREA_TYPE_ELECTRIC_STATION) + getAreaTypeCount(AREA_TYPE_WATER_STATION);
        bool allStations = getAreaTypeCount(AREA_TYPE_ELECTRIC_STATION) > 0 && getAreaTypeCount(AREA_TYPE_WATER_STATION) > 0;

        if (getAreaTypeCount(AREA_TYPE_RESIDENTIAL) / ratio < totalStations && allStations) {
            for (auto* building : zone.buildings) {
                building->level++;
                recomputeBuildingGroupAverage(building->groupId);
            }
        }
        break;
    }
    case AREA_TYPE_COMMERCIAL: {
        int ratio = RATIO_COMMERCES_STATION;
        int totalStations = getAreaTypeCount(AREA_TYPE_PARC) + getAreaTypeCount(AREA_TYPE_ELECTRIC_STATION);
        bool allStations = getAreaTypeCount(AREA_TYPE_PARC) > 0 && getAreaTypeCount(AREA_TYPE_ELECTRIC_STATION) > 0;

        if (getAreaTypeCount(AREA_TYPE_COMMERCIAL) / ratio < totalStations && allStations) {
            for (auto* building : zone.buildings) {
                building->level++;
                recomputeBuildingGroupAverage(building->groupId);
            }
        }
        break;
    }
    case AREA_TYPE_INDUSTRIAL: {
        int ratio = RATIO_INDUSTRIES_STATION;
        int totalStations = getAreaTypeCount(AREA_TYPE_ELECTRIC_STATION) + getAreaTypeCount(AREA_TYPE_SCHOOL) + getAreaTypeCount(AREA_TYPE_WATER_STATION);
        bool allStations = getAreaTypeCount(AREA_TYPE_ELECTRIC_STATION) > 0 && getAreaTypeCount(AREA_TYPE_SCHOOL) > 0 && getAreaTypeCount(AREA_TYPE_WATER_STATION) > 0;

        if (getAreaTypeCount(AREA_TYPE_INDUSTRIAL) / ratio < totalStations && allStations) {
            for (auto* building : zone.buildings) {
                building->level++;
                recomputeBuildingGroupAverage(building->groupId);
            }
        }
        break;
    }
    default:
        break;
    }
}

WorldEntity* World::getWorldTile(const unsigned int mouseX, const unsigned int mouseY) const
{
    if (mouseX >= worldGridWidth
            || mouseY >= worldGridHeight) {
        return nullptr;
    }

    return &worldGrid.get()[((mouseX * worldGridHeight) + mouseY)];
}

int World::getLevel() const
{
    return level;
}

void World::nextLevel()
{
    level++;
}

void World::setLevelFunds(int value)
{
    levelFunds = value;
}

int World::getAreaTypeCount(const eAreaType areaType) const {
    // This function counts the number of each individual zone (1 character)
    // For example: used in the UI to count the individual pieces of road, buildigs ...
    int count = 0;
    if (areaType == AREA_TYPE_ROAD) {
        return roadCount;
    }
    for (int i = 0; i < buildingGroupCount; i++) {
        if (buildingGroups[i].buildings.size() > 0 && buildingGroups[i].buildings.front()->areaType == areaType) {
            if (areaType <= 4) { // Is it a zone or a station ?
                count += buildingGroups[i].buildings.size();
            }
            else {
                count++;
            }
        }
    }
    return count;
}

float World::getAreaTypeMultiplier(const eAreaType areaType) const {

    int areasOfTypeCount = getAreaTypeCount(areaType) + 1;
    int stationsCount = getAreaTypeCount(AREA_TYPE_ELECTRIC_STATION) + getAreaTypeCount(AREA_TYPE_WATER_STATION);
    int buildingsForStation = 0;

    switch (areaType)
    {
    case AREA_TYPE_RESIDENTIAL:
        buildingsForStation = RATIO_RESIDENCES_STATION;
        break;
    case AREA_TYPE_INDUSTRIAL:
        stationsCount = getAreaTypeCount(AREA_TYPE_ELECTRIC_STATION) + getAreaTypeCount(AREA_TYPE_SCHOOL);
        buildingsForStation = RATIO_INDUSTRIES_STATION;
        break;
    case AREA_TYPE_COMMERCIAL:
        stationsCount = getAreaTypeCount(AREA_TYPE_ELECTRIC_STATION) + getAreaTypeCount(AREA_TYPE_PARC);
        buildingsForStation = RATIO_COMMERCES_STATION;
        break;
    default:
        break;
    }

    return stationsCount * buildingsForStation / areasOfTypeCount;
}

int World::allocateBuildingGroupId()
{
    return buildingGroupCount++;
}

int World::allocateRoad()
{
    return roadCount++;
}

int World::allocateBackground()
{
    return backgroundCount++;
}

int World::allocateBuilding()
{
    return buildingCount++;
}

int World::getFunds() const {
    return funds;
}

int World::getDay() const {
    return day;
}

bool World::enoughFunds(const int money)
{
    return funds - money >= 0;
}

bool World::adjacentToRoad(const unsigned int lowX, const unsigned lowY, const unsigned int hiX, const unsigned int hiY) {
    for (unsigned int x = lowX; x < hiX; x++) {
        for (unsigned int y = lowY; y < hiY; y++) {
            if (getTileAtCoordinates(x, y - 1).Type == WorldEntity::ROAD) {
                return true;
            }

            if (getTileAtCoordinates(x, y + 1).Type == WorldEntity::ROAD) {
                return true;
            }

            if (getTileAtCoordinates(x + 1, y).Type == WorldEntity::ROAD) {
                return true;
            }

            if (getTileAtCoordinates(x - 1, y).Type == WorldEntity::ROAD) {
                return true;
            }
        }
    }

    return false;
}

bool World::overlapsArea(const unsigned int lowX, const unsigned lowY, const unsigned int hiX, const unsigned int hiY)
{
    for (unsigned int x = lowX; x < hiX; x++) {
        for (unsigned int y = lowY; y < hiY; y++) {
            auto& tile = getTileAtCoordinates(x, y);
            if (tile.Type == WorldEntity::BUILDING) {
                return true;
            }
            if (tile.Type == WorldEntity::ROAD) {
                return true;
            }
        }
    }

    return false;
}

bool World::isStation(const eAreaType areaType) const
{
    return areaType == AREA_TYPE_ELECTRIC_STATION || areaType == AREA_TYPE_WATER_STATION || areaType == AREA_TYPE_PARC || areaType == AREA_TYPE_SCHOOL;
}

void World::updateFunds(const Transaction transaction, const int money)
{
    switch (transaction)
    {
    case Transaction::CREDIT:
        this->funds += money;
        break;
    case Transaction::DEBIT:
        this->funds -= money;
        break;
    }

}

int World::getAreaTypeCost(const eAreaType areaType) const
{
    switch (areaType)
    {
    case AREA_TYPE_ROAD:
        return ROAD_COST;
    case AREA_TYPE_RESIDENTIAL:
        return RESIDENTIAL_AREA_COST;
    case AREA_TYPE_COMMERCIAL:
        return COMMERCIAL_AREA_COST;
    case AREA_TYPE_INDUSTRIAL:
        return INDUSTRIAL_AREA_COST;
    case AREA_TYPE_ELECTRIC_STATION:
        return 200;
    case AREA_TYPE_WATER_STATION:
        return 300;
    case AREA_TYPE_PARC:
        return 40;
    case AREA_TYPE_SCHOOL:
        return 80;
    default:
        return 0;
    }
}

bool World::isCellOccupied(const unsigned int x, const unsigned int y) const
{
    return (getTileAtCoordinates(x, y).Type != WorldEntity::EMPTY_TILE);
}

void World::recomputeBuildingGroupAverage(const char buildingGroupId)
{
    auto& group = buildingGroups[buildingGroupId];
    group.levelAverage = '0';

    if (group.buildings.empty()) {
        return;
    }

    int groupAverage = 0;
    for (auto* building : group.buildings) {
        groupAverage += building->level;
    }
    groupAverage /= static_cast<int>(group.buildings.size());

    // Clamp Group Level average between 0 and 9
    groupAverage = (groupAverage >= 9) ? 9 : groupAverage;

    // 0x30 + groupAverage (acts as hex offset)
    group.levelAverage = '0' + static_cast<char>(groupAverage);
}

