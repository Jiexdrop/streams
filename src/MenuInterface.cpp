#include "MenuInterface.h"
#include "GameStateManager.h"

MenuInterface::MenuInterface()
{
    setup();
}

void MenuInterface::setup(){
    // Create menu Buttons
    buttonsNames[0] = "Play";
    buttonsNames[1] = "Quit";

    for(int i = 0; i < Buttons::COUNT; i++){
        buttons[i] = Button(((i+1) * 10), 16, 8, 4, buttonsNames[i]);
    }

    // Get Menu Title string
    int k = 0;
    std::ifstream titleText("assets/title.txt");
    for(std::string line; getline( titleText, line ); )
    {
        titleStrings[k++] = line;
    }

    k = 0;
    std::ifstream titleCityText("assets/city.txt");
    for(std::string line; getline( titleCityText, line ); )
    {
        if(k<NB_CITY_LINES) titleCityStrings[k++] = line;
    }
}


void MenuInterface::render(GameStateManager * gm, ASCIIRenderer *renderer, InputManager *im) {
    drawHUDBorders( renderer, INTERFACE_FILL_COLOR );

    // Draw title
    for(int i = 0; i < NB_CITY_LINES ; i++)
    {
        renderer->drawText(1, i+1, titleCityStrings[i], INTERFACE_FILL_COLOR );
    }
    for(int i = 0; i < NB_TITLE_LINES ; i++)
    {
        renderer->drawText(1, i+1, titleStrings[i], INTERFACE_FILL_COLOR );
    }

    for(int i = 0; i < Buttons::COUNT; i++){
        buttons[i].checked = buttons[i].checkCollision(im->getCursorX(), im->getCursorY(), 1, 1);
        buttons[i].render(renderer);

        if(buttons[i].checked && im->isPressed()){
            if(buttonsNames[i] == "Play" ) {
                gm->setState(State::GAME);
            }
            if(buttonsNames[i] == "Quit" ) {
                gm->setState(State::QUIT);
            }
        }
    }

}
