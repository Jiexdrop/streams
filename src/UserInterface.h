#ifndef USERINTERFACE_H
#define USERINTERFACE_H

#include "ASCIIRenderer.h"
#include "Inputmanager.h"
#include "Button.h"
#include <windows.h>
#include <iostream>
#include <map>
#include <stdlib.h>
#include <conio.h>
#include "World.h"
#include "Interface.h"
#include "Window.h"

enum Tools
{
	NONE = 0,

    ROAD,
    RESIDENTIAL,
    INDUSTRIAL,
    COMMERCIAL,

	WATER_STATION,
	ELECTRIC_STATION,
	PARC,
	SCHOOL,

	REMOVE,

	COUNT
};


enum Windows
{
    TUTORIAL,
    WIN,

    WINDOW_COUNT
};


class UserInterface : Interface
{
public:
    UserInterface();

    void render(ASCIIRenderer *renderer, World *logicWorld, InputManager* im, const float deltaTime);
    void update(World *world, InputManager* im);

    void setup();

private:
    Button buttons[Tools::COUNT];


    std::string cursorPos;

    COORD startPress;
    COORD endPress;

    Tools tool;

    Window windows[Windows::WINDOW_COUNT];

    unsigned int selectedZoneLowX;
    unsigned int selectedZoneLowY;
    unsigned int selectedZoneHiX;
    unsigned int selectedZoneHiY;

    void drawInterfaceLayout(ASCIIRenderer* renderer);
    void winInterfaceUpdate(World * logicWorld);

    bool win;
};



#endif // USERINTERFACE_H
