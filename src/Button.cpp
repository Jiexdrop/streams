#include "Button.h"

Button::Button(int x, int y, int width, int height, std::string name)
    : x(x)
    , y(y)
    , width(width)
    , height(height)
    , name(name)
    , cost("")
{
    checked = false;
}

Button::Button(){
    x = 0;
    y = 0;
    width = 0;
    height = 0;
    name = "";
    checked = false;
}

void Button::render(ASCIIRenderer *renderer)
{
    if(checked){
        renderer->drawCenteredRectangle( x, y, width, height, ' ', BUTTON_CHECKED_FILL_COLOR);
        renderer->drawText(x - width/4, y - height/4, name,  BUTTON_CHECKED_FILL_COLOR);
        renderer->drawText(x - width/4, y - height/4 + 1, cost,  BUTTON_CHECKED_FILL_COLOR);
    } else {
        renderer->drawCenteredRectangle( x, y, width, height, ' ', BUTTON_FILL_COLOR);
        renderer->drawText(x - width/4, y - height/4, name, BUTTON_FILL_COLOR);
        renderer->drawText(x - width/4, y - height/4 + 1, cost, BUTTON_FILL_COLOR);
    }
}

bool Button::checkCollision(int x1, int y1, int w1, int h1){
    return x1 < x + width/2 &&
            x1 + w1 > x - width/2 &&
            y1 < y + height/2 &&
            h1 + y1 > y - height/2;
}

void Button::updateCost(const int cost)
{
	this->cost = std::to_string(cost);
}
