#include "ASCIIRenderer.h"

#include <conio.h>

#undef min
#include <algorithm>

ASCIIRenderer::ASCIIRenderer()
    : buffers{ nullptr, nullptr }
    , currentBufferIndex( 0 )
    , consoleHandle( nullptr )
    , bufferSize{ 0, 0 }
    , bufferWidth( 0u )
    , bufferHeight( 0u )
{

}

ASCIIRenderer::~ASCIIRenderer()
{
    currentBufferIndex = 0;
    consoleHandle = nullptr;
    bufferSize = { 0, 0 };
    bufferWidth = 0;
    bufferHeight = 0;
}

void ASCIIRenderer::create( const int screenWidth, const int screenHeight)
{
    bufferWidth = screenWidth;
    bufferHeight = screenHeight;

    consoleHandle = static_cast< HANDLE >( GetStdHandle( STD_OUTPUT_HANDLE ) );

    bufferSize.X = static_cast< SHORT >( bufferWidth );
    bufferSize.Y = static_cast< SHORT >( bufferHeight );

    for ( int bufferIdx = 0; bufferIdx < BUFFER_COUNT; bufferIdx++ ) {
        buffers[bufferIdx].reset( new CHAR_INFO[bufferWidth * bufferHeight]);
        memset( buffers[bufferIdx].get(), 0, sizeof( CHAR_INFO ) * bufferWidth * bufferHeight );
    }
}

void ASCIIRenderer::fillPixel( const unsigned int x, const unsigned int y, const char asciiValue, const int flags, const bool blend )
{
    if ( x >= bufferWidth
      || y >= bufferHeight ) {
        return;
    }

    CHAR_INFO* currentBuffer = buffers[currentBufferIndex].get();

    const int bufferOffset = ( ( y * bufferWidth ) + x );

    currentBuffer[bufferOffset].Char.AsciiChar = asciiValue;
    if ( blend ) {
        auto flagsBackgroundLess = flags & 0x00FF;

        currentBuffer[bufferOffset].Attributes |= flagsBackgroundLess;
    } else {
        currentBuffer[bufferOffset].Attributes = flags;
    }
}

void ASCIIRenderer::drawLine( const unsigned int x0, const unsigned int y0, const unsigned int x1, const unsigned int y1, const char fillValue, const int flags )
{
    auto x = x0;
    auto y = y0;
    int dy = y1 - y0;
    int dx = x1 - x0;
    int stepX = 0;
    int stepY = 0;

    if ( dy < 0 ) {
        dy = -dy;  
        stepY = -1;
    } else {
        stepY = 1;
    }

    if ( dx < 0 ) {
        dx = -dx;  
        stepX = -1;
    } else {
        stepX = 1;
    }

    dy <<= 1;
    dx <<= 1;

    fillPixel( x, y, fillValue, flags );

    // Figure out line's direction and step each pixel on the line
    if ( dx > dy ) {
        int fraction = dy - ( dx >> 1 );

        while ( x != x1 ) {
            if ( fraction >= 0 ) {
                y += stepY;
                fraction -= dx;
            }

            x += stepX;
            fraction += dy;

            fillPixel( x, y, fillValue, flags );
        }
    } else {
        int fraction = dx - ( dy >> 1 );

        while ( y != y1 ) {
            if ( fraction >= 0 ) {
                x += stepX;
                fraction -= dy;
            }

            y += stepY;
            fraction += dx;

            fillPixel( x, y, fillValue, flags );
        }
    }
}

void ASCIIRenderer::drawRectangle( const unsigned int lowX, const unsigned int lowY, const unsigned int hiX, const unsigned int hiY, const char fillValue, const int flags )
{
    for ( unsigned int yIdx = lowY; yIdx < hiY; yIdx++ ) {
        for ( unsigned int xIdx = lowX; xIdx < hiX; xIdx++ ) {
            fillPixel( xIdx, yIdx, fillValue, flags );
        }
    }
}

void ASCIIRenderer::drawBlendedRectangle( const unsigned int lowX, const unsigned int lowY, const unsigned int hiX, const unsigned int hiY, const char fillValue, const int flags )
{
    for ( unsigned int yIdx = lowY; yIdx < hiY; yIdx++ ) {
        for ( unsigned int xIdx = lowX; xIdx < hiX; xIdx++ ) {
            fillPixel( xIdx, yIdx, fillValue, flags, true );
        }
    }
}

void ASCIIRenderer::drawCenteredRectangle( const unsigned int centerX, const unsigned int centerY, const unsigned int width, const unsigned int height, const char fillValue, const int flags ){
    int lowX = centerX - width/2;
    int lowY = centerY - height/2;
    int hiX = centerX + width/2;
    int hiY = centerY + height/2;
    drawRectangle(lowX, lowY, hiX, hiY, fillValue, flags);
}

void ASCIIRenderer::drawText( const unsigned int x, const unsigned int y, const std::string& text, const int flags )
{
    unsigned int rowEnd = std::min( bufferHeight, y + 1u );

    unsigned int textEndOffset = static_cast< unsigned int >( x + text.length() );

    // Fit text on screen based on its x offset
    while ( textEndOffset >= bufferWidth ) {
        rowEnd++;
        textEndOffset -= ( bufferWidth - x );
    }

    unsigned int colEnd = std::min( bufferWidth, static_cast< unsigned int >( x + text.length() ) );

    int charIndex = 0;
    for ( unsigned int posY = y; posY < rowEnd; posY++ ) {
        unsigned int rowColEnd = ( posY == ( rowEnd - 1 ) ) ? colEnd % bufferWidth : colEnd;
        for ( unsigned int posX = x; posX < rowColEnd; posX++ ) {
            // Handle special chars
            const char charToPrint = text.at( charIndex++ );

            if ( charToPrint == '\t' ) {
                posX += 4;
                continue;
            } else if ( charToPrint == '\n' ) {
                posY++;
                continue;
            }

            fillPixel( posX, posY, charToPrint, flags );
        }
    }
}

void ASCIIRenderer::drawUiText(const unsigned int x, const unsigned int y, const std::string& text, const unsigned int width, const int flags)
{
    unsigned int rowEnd = std::min( bufferHeight, y + 1u );

    unsigned int textEndOffset = static_cast< unsigned int >( x + text.length() );

    // Fit text on screen based on its x offset
    while ( textEndOffset >= width ) {
        rowEnd++;
        textEndOffset -= ( width - x );
    }

    unsigned int colEnd = std::min( width, static_cast< unsigned int >( x + text.length() ) );

    int charIndex = 0;

    for ( unsigned int posY = y; posY < rowEnd; posY++ ) {
        unsigned int rowColEnd = ( posY == ( rowEnd - 1 ) ) ? colEnd % width : colEnd;
        for ( unsigned int posX = x; posX < rowColEnd; posX++ ) {
            // Handle special chars
            const char charToPrint = text.at( charIndex++ );

            if ( charToPrint == '\t' ) {
                posX += 4;
                continue;
            } else if ( charToPrint == '\n' ) {

                posY++;
                continue;
            }


            fillPixel( posX, posY, charToPrint, flags );
        }
    }
}

void ASCIIRenderer::clearBuffer()
{
    clearBuffer( currentBufferIndex );
}

void ASCIIRenderer::clearBuffer( const char clearValue, const int flags )
{
    for ( unsigned int i = 0; i < ( bufferWidth * bufferHeight ); ++i ) {
        ( buffers[currentBufferIndex].get() )[i].Char.AsciiChar = clearValue;
        ( buffers[currentBufferIndex].get() )[i].Attributes = flags;
    }
}

void ASCIIRenderer::present()
{
    auto bufferRegion = getWholeBufferRegion();

    WriteConsoleOutput( 
        consoleHandle,
        reinterpret_cast<PCHAR_INFO>( buffers[currentBufferIndex].get() ), 
        bufferSize,
        { 0, 0 }, 
        &bufferRegion 
    );

    currentBufferIndex++;
    if ( currentBufferIndex >= BUFFER_COUNT ) {
        currentBufferIndex = 0;
    }
}

SMALL_RECT ASCIIRenderer::getWholeBufferRegion() const
{
    SMALL_RECT rcRegion;
    rcRegion.Left = 0;
    rcRegion.Top = 0;
    rcRegion.Right = ( bufferWidth - 1 );
    rcRegion.Bottom = ( bufferHeight - 1 );

    return rcRegion;
}

void ASCIIRenderer::clearBuffer( const int bufferIndex )
{
    memset( buffers[bufferIndex].get(), 0, ( sizeof( CHAR_INFO ) * bufferWidth * bufferHeight ) );
}

unsigned int ASCIIRenderer::getBufferWidth() const
{
    return bufferWidth;
}

unsigned int ASCIIRenderer::getBufferHeight() const
{
    return bufferHeight;
}

