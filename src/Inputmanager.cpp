#include "Inputmanager.h"

#include <iostream>

InputManager::InputManager()
    : cursor{ 0, 0 }
{

}

void InputManager::readInput(const float deltaTime) {
    INPUT_RECORD InputRecord;
    DWORD Events;
    HANDLE consoleInput = static_cast< HANDLE >( GetStdHandle( STD_INPUT_HANDLE ) );

    DWORD eventCount = 0;
    if ( GetNumberOfConsoleInputEvents( consoleInput, &eventCount ) == FALSE ) {
        // Shouldn't be here unless the func call failed
        return;
    }

    while ( eventCount > 0 ) {

        ReadConsoleInput( consoleInput, &InputRecord, 1, &Events );

        switch ( InputRecord.EventType ) {
        case KEY_EVENT:
            switch ( InputRecord.Event.KeyEvent.wVirtualKeyCode ) {
            case VK_ESCAPE:
                //g_ShouldQuit = true;
                break;
            }
            break;

        case MOUSE_EVENT:
            cursor.X = InputRecord.Event.MouseEvent.dwMousePosition.X;
            cursor.Y = InputRecord.Event.MouseEvent.dwMousePosition.Y;

            pressed = ( InputRecord.Event.MouseEvent.dwButtonState == FROM_LEFT_1ST_BUTTON_PRESSED );
            released = !pressed;
            break;
        default:
            break;
        }

        eventCount--;
    }

}


unsigned int InputManager::getCursorX() const {
    return cursor.X;
}

unsigned int InputManager::getCursorY() const {
    return cursor.Y;
}

bool InputManager::isPressed() const {
    return pressed;
}

bool InputManager::isReleased() const {
    return !pressed;
}
